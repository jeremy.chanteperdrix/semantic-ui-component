import { SemanticUiComponentPage } from './app.po';

describe('semantic-ui-component App', () => {
  let page: SemanticUiComponentPage;

  beforeEach(() => {
    page = new SemanticUiComponentPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
