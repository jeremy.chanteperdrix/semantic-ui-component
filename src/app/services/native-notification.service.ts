import { Injectable } from '@angular/core';

declare const Notification: any;

@Injectable()
export class NativeNotificationService {
  constructor() {
  }

  requestPermission(callback) {
    if (Notification.permission !== 'granted') {
      Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
          Notification.permission = status;
        }
        if (typeof callback === 'function') {
          callback(Notification.permission);
        }
      });
    } else {
      if (typeof callback === 'function') {
        callback(Notification.permission);
      }
    }
  }

  isGranted() {
    return Notification.permission === 'granted';
  }

  open(options: NativeNoti) {
    options.timeout = options.timeout || 5;
    options.icon = options.icon || '/assets/images/nnoti_icon.png';
    this.requestPermission(() => {
      if (this.isGranted()) {
        const noti = new Notification(options.title, options);
        setTimeout(() => noti.close(), options.timeout * 1000);
      }
    });
  }
}

export interface NativeNoti {
  title: string;
  body: string;
  data?: string;
  badge?: string;
  icon?: string;
  image?: string;
  timeout?: number;
}
