import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class SemanticUiCalendarService {
  getOptions(type = 'date') {
    return {
      type,
      ampm: false,
      firstDayOfWeek: 1,
      formatter: {
        date: (date, settings) => moment(date).format('DD/MM/YYYY')
      },
      text: {
        days: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        months: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai',
          'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: `Aujourd'hui`,
        now: 'Maintenant',
        am: 'MA',
        pm: 'AM'
      }
    };
  }
}
