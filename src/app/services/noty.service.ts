import { Injectable } from '@angular/core';

declare const Noty: any;

@Injectable()
export class NotyService {
  constructor() {
    Noty.overrideDefaults({
      layout: 'bottomRight',
      closeWith: ['click'],
      timeout: 3000,
      theme: 'semanticui'
    });
  }

  alert(text) {
    new Noty({ type: 'alert', text }).show();
  }

  success(text) {
    new Noty({ type: 'success', text }).show();
  }

  error(text) {
    new Noty({ type: 'error', text }).show();
  }

  warning(text) {
    new Noty({ type: 'warning', text }).show();
  }

  info(text) {
    new Noty({ type: 'info', text }).show();
  }
}
