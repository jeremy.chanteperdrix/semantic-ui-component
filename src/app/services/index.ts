import { SemanticUiCalendarService } from './semantic-ui-calendar.service';
import { NotyService } from './noty.service';
import { NativeNotificationService } from './native-notification.service';

export const SemanticUiServices: Array<any> = [
  SemanticUiCalendarService,
  NotyService,
  NativeNotificationService
];
