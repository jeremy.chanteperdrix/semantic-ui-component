import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { SemanticUiModules } from './modules';
import { SemanticUiServices } from './services';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ].concat(SemanticUiModules),
  providers: [].concat(SemanticUiServices),
  bootstrap: [AppComponent]
})
export class AppModule {
}
