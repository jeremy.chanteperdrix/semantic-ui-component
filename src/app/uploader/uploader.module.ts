import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploaderComponent } from './uploader.component';
import { FormsModule } from '@angular/forms';
import { SemanticUiProgressModule } from '../modules/semantic-ui-progress/semantic-ui-progress.module';
@NgModule({
  declarations: [UploaderComponent],
  imports: [CommonModule, FormsModule, SemanticUiProgressModule],
  exports: [UploaderComponent]
})
export class UploaderModule {
}
