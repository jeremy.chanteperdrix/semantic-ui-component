import {
  AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output,
  QueryList,
  ViewChild, ViewChildren
} from '@angular/core';
import { NotyService } from '../services/noty.service';

declare const $: any;

@Component({
  selector: 'app-uploader',
  templateUrl: 'uploader.component.html',
  styleUrls: ['uploader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploaderComponent implements AfterContentInit {
  @Input('options') options: any;

  @Output('afterSubmit') afterSubmit: EventEmitter<any[]> = new EventEmitter();

  @ViewChild('inputFiles') inputFiles: ElementRef;
  @ViewChild('modalViewer') modalViewer: ElementRef;
  @ViewChildren('link') links: QueryList<ElementRef>;

  public files: any[] = [];
  public fileReader = new FileReader();
  public modal: any = { imageSrc: null, description: null };
  public disabled = true;
  public loading = false;
  public defaultOptions = {
    fileType: {
      display: true,
      locked: false,
      choices: [],
      defaultChoice: null,
      label: 'Type',
    },
    imageViewer: true,
    url: null
  };

  constructor(private noty: NotyService, private cdr: ChangeDetectorRef) {
  }

  onSubmit() {
    this.loading = true;
    setTimeout(() => {
      this.noty.success('Upload terminé');
      this.loading = false;
      this.afterSubmit.emit(this.files);
      this.files = [];
      this.checkForEnable();
    }, 5000);
  }

  ngAfterContentInit() {
    this.options = Object.assign(Object.assign({}, this.defaultOptions), this.options || {});
    this.options.fileType = Object.assign(Object.assign({}, this.defaultOptions.fileType), this.options.fileType || {});
  }

  addFiles() {
    this.inputFiles.nativeElement.click();
  }

  deleteFile(index) {
    this.files.splice(index, 1);
    this.checkForEnable();
  }

  deleteAll() {
    this.files = [];
    this.checkForEnable();
  }

  associateFiles() {
    this.readFile(0);
  }

  readFile(index) {
    if (index >= this.inputFiles.nativeElement.files.length) {
      return;
    }
    const file = this.inputFiles.nativeElement.files[index];
    this.fileReader.onload = () => {
      const bin = this.fileReader.result;
      this.files.push({
        name: file.name,
        src: bin,
        type: this.options.fileType.defaultChoice,
        displayable: true
      });
      this.checkForEnable();
      this.readFile(index + 1);
    };
    this.fileReader.readAsDataURL(file);
  }

  imgOnload($event, i) {
    $('.ui.dropdown').dropdown();
    this.links.toArray()[i].nativeElement.click();
  }

  imgError(file) {
    $('.ui.dropdown').dropdown();
    file.displayable = false;
  }

  initModal(file) {
    if (this.modal.imageSrc === file.src) {
      this.showModal();
    }
    this.modal = {
      imageSrc: file.src,
      description: file.name
    };
  }

  showModal() {
    if (this.options.imageViewer) {
      $(this.modalViewer.nativeElement).modal('show');
    }
  }

  checkForEnable() {
    this.disabled = this.files.length === 0;
    this.cdr.markForCheck();
  }

  onChangeType(e, file) {
    file.type = e.target.value;
  }

  closeModal() {
    $(this.modalViewer.nativeElement).modal('hide');
  }
}
