import {
  AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output,
  ViewChild
} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

declare const $: any;

@Component({
  selector: 'sem-input',
  templateUrl: './semantic-ui-input.component.html',
  styleUrls: ['./semantic-ui-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SemanticUiInputComponent implements AfterViewInit {
  @ViewChild('input') input: ElementRef;
  @ViewChild('progress') progress: ElementRef;

  @Input() model: any;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  @Input() inForm = false;

  @Input() duration = 300;

  subModelChange: Subject<any> = new Subject();
  $progress: any;

  constructor() {
    this.subModelChange
      .debounceTime(this.duration)
      .distinctUntilChanged()
      .subscribe(input => {
        this.modelChange.next(input);
        this.$progress.progress('reset');
      });
  }

  ngAfterViewInit() {
    this.$progress = $(this.progress.nativeElement);
  }

  onChange(input) {
    this.subModelChange.next(input);
    this.$progress.progress({
      duration: this.duration,
      percent: 100,
      autoSuccess: false
    });
  }

}
