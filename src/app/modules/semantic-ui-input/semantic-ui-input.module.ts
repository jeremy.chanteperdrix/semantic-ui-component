import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiInputComponent } from './semantic-ui-input.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [SemanticUiInputComponent],
  exports: [SemanticUiInputComponent]
})
export class SemanticUiInputModule { }
