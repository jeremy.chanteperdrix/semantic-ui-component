import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticUiInputComponent } from './semantic-ui-input.component';

describe('SemanticUiInputComponent', () => {
  let component: SemanticUiInputComponent;
  let fixture: ComponentFixture<SemanticUiInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticUiInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticUiInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
