import {
  AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef,
  Component, ElementRef, EventEmitter, forwardRef, HostBinding, Input,
  OnChanges, OnInit, Output, QueryList, SimpleChanges, ViewChild, ViewChildren
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

declare const $: any;

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SemanticUiDropdownComponent),
  multi: true
};

@Component({
  selector: 'app-sem-dropdown',
  templateUrl: './semantic-ui-dropdown.component.html',
  styleUrls: ['./semantic-ui-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class SemanticUiDropdownComponent implements AfterViewInit, OnChanges, ControlValueAccessor, OnInit {
  @ViewChild('dropdown') dropdown: ElementRef;
  @ViewChild('label') label: ElementRef;
  @ViewChildren('option') options: QueryList<ElementRef>;

  @Input() placeholder = 'Choisir';
  @Input() inForm = false;
  @Input() class: string;
  @Input() name: string;
  @Input() id: string;
  @Input() onOverOpen = false;
  @Input() forceSelection = false;

  @Input() data: any = [];
  @Input() display: any = null;
  @Input() link: any = null;

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  @HostBinding('class.field') classes;

  private onTouchedCallback: () => void;
  private onChangeCallback: (_: any) => void;

  private _model: any;

  public $dropdown: any;
  protected labelDisplayable = true;
  protected loading = false;

  constructor(private cdr: ChangeDetectorRef) {
  }

  get model() {
    return this._model;
  }

  set model(value: any) {
    if (value !== this._model) {
      this._model = value;
      this.onChangeCallback(value);
    }
  }

  writeValue(obj: any): void {
    if (obj !== this._model) {
      this._model = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  ngOnInit() {
    if (!this.data) {
      this.loading = true;
    }
    if (this.inForm) {
      this.classes = 'field';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['data']) {
      if (this.$dropdown) {
        this.refreshDropdown();
      }
    }
  }

  ngAfterViewInit() {
    this.$dropdown = $(this.dropdown.nativeElement).dropdown({
      forceSelection: this.forceSelection
    });
    this.labelDisplayable = !!this.label.nativeElement.innerText.trim().length;
    this.options.changes.subscribe(this.refreshDropdown.bind(this));
  }

  private refreshDropdown() {
    this.loading = false;
    this.$dropdown.dropdown('refresh');
    this.$dropdown.dropdown('set selected', this.model);
  }

  protected onClick(value) {
    this.model = value[this.link];
    this.onChange.emit(value);
  }

  protected onMouseover() {
    if (this.onOverOpen) {
      this.$dropdown.dropdown('show');
    }
  }

  protected onMouseleave() {
    if (this.onOverOpen) {
      this.$dropdown.dropdown('hide');
    }
  }

  protected onKeydown(event: KeyboardEvent) {
    if (event.keyCode === 27 && this.$dropdown.dropdown('is hidden')) {
      this.$dropdown.dropdown('clear');
    }
  }
}
