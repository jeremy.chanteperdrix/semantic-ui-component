import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticUiDropdownComponent } from './semantic-ui-dropdown.component';

describe('SemanticUiDropdownComponent', () => {
  let component: SemanticUiDropdownComponent;
  let fixture: ComponentFixture<SemanticUiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticUiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticUiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
