import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiDropdownComponent } from './semantic-ui-dropdown.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [SemanticUiDropdownComponent],
  exports: [SemanticUiDropdownComponent]
})
export class SemanticUiDropdownModule { }
