import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiDatatableComponent } from './semantic-ui-datatable.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SemanticUiDatatableComponent],
  exports: [SemanticUiDatatableComponent]
})
export class SemanticUiDatatableModule {
}
