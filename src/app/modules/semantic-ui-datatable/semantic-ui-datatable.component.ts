import {
  AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input,
  OnInit,
  ViewChild, ViewEncapsulation
} from '@angular/core';
import * as moment from 'moment';
import { cloneDeep } from 'lodash';

declare const $: any;

@Component({
  selector: 'sem-datatable',
  templateUrl: './semantic-ui-datatable.component.html',
  styleUrls: ['./semantic-ui-datatable.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class SemanticUiDatatableComponent implements OnInit, AfterViewInit {
  @ViewChild('datatable') datatable: ElementRef;
  @ViewChild('exportButtons') exportButtons: ElementRef;
  @ViewChild('thead') thead: ElementRef;

  @Input() settings: DatatablesSettings;

  $datatable: any;
  initComplete = false;

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => this.initDataTables());
  }

  private initDataTables() {
    this.$datatable = $(this.datatable.nativeElement).DataTable(this.getSettings());
  }

  private getSettings() {
    const settings: any = {
      language: {
        url: '/assets/internationalisation/datatable/french.json'
      },
      select: {
        style: 'single',
        className: 'active'
      },
      dom: `<'export'B><'search-display'<'search'f><'display'l>>t<'info-page'<'info'i><'page'p>>`,
      buttons: [
        {
          extend: 'excel',
          text: (((this.settings || {}).buttons || {}).excel || <DatatablesButton>{}).text || 'EXCEL',
          filename: (((this.settings || {}).buttons || {}).excel || <DatatablesButton>{}).filename || `Geop_${moment().format('YYYYMMDD')}`
        }, {
          extend: 'csv',
          text: (((this.settings || {}).buttons || {}).csv || <DatatablesButton>{}).text || 'CSV',
          filename: (((this.settings || {}).buttons || {}).csv || <DatatablesButton>{}).filename || `Geop_${moment().format('YYYYMMDD')}`
        }, {
          extend: 'pdf',
          text: (((this.settings || {}).buttons || {}).pdf || <DatatablesButton>{}).text || 'PDF',
          filename: (((this.settings || {}).buttons || {}).pdf || <DatatablesButton>{}).filename || `Geop_${moment().format('YYYYMMDD')}`,
          orientation: (((this.settings || {}).buttons || {}).pdf || <DatatablesButton>{}).orientation || 'landscape',
          pageSize: (((this.settings || {}).buttons || {}).pdf || <DatatablesButton>{}).pageSize || 'A4'
        }, {
          extend: 'colvis',
          text: 'Colonnes'
        }
      ],
      initComplete: () => {
        this.initComplete = true;
        this.cdr.markForCheck();
        // On regarde si on a des input dans la balise thead
        this.$datatable.columns().every((i) => {
          $('input', this.$datatable.columns([i]).footer()).on('keyup', (element) => {
            if (this.$datatable.columns([i]).search() !== element.target.value) {
              this.$datatable
                .columns([i])
                .search(element.target.value)
                .draw();
            }
          });
        });
      }
    };
    // Si on a une options de définition de colonne
    if ((this.settings || {}).columnDefs) {
      settings.columnDefs = cloneDeep(this.settings.columnDefs);
    }
    // Si on a un ordre prédéfini
    if ((this.settings || {}).order) {
      settings.order = cloneDeep(this.settings.order);
    }
    // Si on a une longueur d'affichage
    if ((this.settings || {}).displayLength) {
      settings.displayLength = cloneDeep(this.settings.displayLength);
    }
    // Si on a l'option de groupage de ligne
    if ((this.settings || {}).rowGroup) {
      // On construit le callback
      settings.drawCallback = () => {
        const rows = this.$datatable.rows({ page: 'current' }).nodes();
        let last = null;
        this.$datatable.column(this.settings.rowGroup.columnIndex, { page: 'current' }).data().each((group, i) => {
          if (last !== group) {
            $(rows).eq(i).before(this.settings.rowGroup.template.replace('#', group));
            last = group;
          }
        });
      };
    }
    return settings;
  }

}

export interface DatatablesSettings {
  buttons?: {
    excel?: DatatablesButton,
    csv?: DatatablesButton,
    pdf?: DatatablesButton
  };
  rowGroup?: DatatablesRowGroup;
  columnDefs?: DatatablesColumnDefs[];
  order?: any[];
  displayLength?: number;
}

export interface DatatablesButton {
  filename: string;
  text: string;
  orientation: 'portrait' | 'landscape';
  pageSize: 'A3' | 'A4' | 'A5' | 'LEGAL' | 'LETTER' | 'TABLOID';
}

export interface DatatablesColumnDefs {
  visible: boolean;
  targets: number;
}

export interface DatatablesRowGroup {
  columnIndex: number;
  template: string;
}
