import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticUiDatatableComponent } from './semantic-ui-datatable.component';

describe('SemanticUiDatatableComponent', () => {
  let component: SemanticUiDatatableComponent;
  let fixture: ComponentFixture<SemanticUiDatatableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticUiDatatableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticUiDatatableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
