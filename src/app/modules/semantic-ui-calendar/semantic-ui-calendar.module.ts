import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiCalendarComponent } from './semantic-ui-calendar.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [SemanticUiCalendarComponent],
  exports: [SemanticUiCalendarComponent]
})
export class SemanticUiCalendarModule { }
