import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticUiCalendarComponent } from './semantic-ui-calendar.component';

describe('SemanticUiCalendarComponent', () => {
  let component: SemanticUiCalendarComponent;
  let fixture: ComponentFixture<SemanticUiCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticUiCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticUiCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
