import {
  AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges,
  OnInit,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import * as moment from 'moment';

declare const $: any;

@Component({
  selector: 'sem-calendar',
  templateUrl: './semantic-ui-calendar.component.html',
  styleUrls: ['./semantic-ui-calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SemanticUiCalendarComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() model: any;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  @Input() format = 'DD/MM/YYYY';

  @Input() startCalendar: SemanticUiCalendarComponent;
  @Output() startCalendarChange: EventEmitter<any> = new EventEmitter();

  @Input() endCalendar: SemanticUiCalendarComponent;
  @Output() endCalendarChange: EventEmitter<any> = new EventEmitter();

  @Input() type = 'date';

  @ViewChild('calendar') calendar: ElementRef;
  @ViewChild('calendarInline') calendarInline: ElementRef;

  @Output() onInit: EventEmitter<ElementRef> = new EventEmitter();

  @Input() inForm = false;

  @Input() inline = false;

  @ViewChild('label') label: ElementRef;
  labelDisplayable = true;
  $calendar: any;
  isInit= false;

  settings: any = {
    type: this.type,
    ampm: false,
    firstDayOfWeek: 1,
    formatter: {
      date: (date, settings) => moment(date).format(this.format)
    },
    text: {
      days: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
      months: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai',
        'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
      monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
      today: `Aujourd'hui`,
      now: 'Maintenant',
      am: 'MA',
      pm: 'AM'
    },
    inline: this.inline,
    onChange: this.onChange.bind(this)
  };

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('model' in changes) {
      if (JSON.stringify(changes['model'].currentValue) !== JSON.stringify(changes['model'].previousValue) && this.calendar) {
        setTimeout(() => this.setDefaultDate());
      }
    }
    if ('startCalendar' in changes) {
      this.initCalendar();
    }
    if ('endCalendar' in changes) {
      this.initCalendar();
    }
    if ('type' in changes) {
      this.settings.type = this.type;
      this.initCalendar();
    }
  }

  ngAfterViewInit() {
    this.isInit = true;
    this.initCalendar();
    this.labelDisplayable = !!this.label.nativeElement.innerText.trim().length;
  }

  initCalendar() {
    if (this.isInit) {
      // On check si des dates de début doivent être initialisées
      if (this.startCalendar) {
        this.settings.startCalendar = this.startCalendar.$calendar;
      } else if (this.endCalendar) {
        this.settings.endCalendar = this.endCalendar.$calendar;
      }
      if (this.inline) {
        this.$calendar = $(this.calendarInline.nativeElement).calendar(this.settings);
      } else {
        this.$calendar = $(this.calendar.nativeElement).calendar(this.settings);
      }
      this.onInit.emit(this.$calendar);
      this.setDefaultDate();
    }
  }

  onChange(date) {
    this.model = moment(date, this.format).toDate();
    this.modelChange.next(this.model);
    return true;
  }

  setDefaultDate() {
    this.$calendar.calendar('set date', moment(this.model, this.format || null).toDate());
  }
}
