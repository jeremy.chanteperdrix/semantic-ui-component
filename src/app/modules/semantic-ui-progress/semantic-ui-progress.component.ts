import {
  AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';

declare const $: any;

@Component({
  selector: 'sem-progress',
  templateUrl: './semantic-ui-progress.component.html',
  styleUrls: ['./semantic-ui-progress.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SemanticUiProgressComponent implements AfterViewInit, OnChanges {
  @Input('model') model: any;
  @Input('total') total: number;
  @Input('settings') settings: Settings = {
    color: '',
    autoSuccess: false,
    showTotal: false
  };

  @ViewChild('progress') progress: ElementRef;

  private $progress: any;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if ('model' in changes) {
      if (this.$progress) {
        this.$progress.progress('set progress', this.model);
      }
    }
  }

  ngAfterViewInit() {
    this.init();
  }

  init() {
    this.$progress = $(this.progress.nativeElement).progress({
      autoSuccess: false
    });
  }

}

interface Settings {
  color?: string;
  autoSuccess?: boolean;
  showTotal?: boolean;
}
