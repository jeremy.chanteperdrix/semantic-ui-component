import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticUiProgressComponent } from './semantic-ui-progress.component';

describe('SemanticUiProgressComponent', () => {
  let component: SemanticUiProgressComponent;
  let fixture: ComponentFixture<SemanticUiProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticUiProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticUiProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
