import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiProgressComponent } from './semantic-ui-progress.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SemanticUiProgressComponent],
  exports: [SemanticUiProgressComponent]
})
export class SemanticUiProgressModule { }
