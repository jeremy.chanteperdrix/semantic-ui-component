import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, ContentChildren, ElementRef, Input, QueryList,
  ViewChild, ViewChildren
} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import { ModalHeaderComponent } from './modal-header/modal-header.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { ModalButtonComponent } from './modal-button/modal-button.component';

declare const $: any;

@Component({
  selector: 'sem-modal',
  templateUrl: './semantic-ui-modal.component.html',
  styleUrls: ['./semantic-ui-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SemanticUiModalComponent implements AfterViewInit {
  @Input() closable = true;
  @Input() allowMultiple = true;
  @Input() nextModal: SemanticUiModalComponent;
  @Input() classes: string;

  @ViewChild('modal') private _modal: ElementRef;
  @ViewChild('approveButton') approveButton: ElementRef;
  @ViewChild('denyButton') denyButton: ElementRef;
  @ContentChild(ModalHeaderComponent) appModalHeader: ModalHeaderComponent;
  @ContentChild(ModalContentComponent) appModalContent: ModalContentComponent;
  @ContentChildren(ModalButtonComponent) actions: QueryList<ModalButtonComponent>;

  private subjApprove: Subject<any> = new Subject();
  private subjDeny: Subject<any> = new Subject();
  approve: Observable<any> = Observable.from(this.subjApprove);
  deny: Observable<any> = Observable.from(this.subjDeny);

  $modal: any;
  data: any;
  displayActions = false;

  private isInit = false;
  private isAttached = false;

  isCoupled = false;

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.$modal = $(this._modal.nativeElement).modal({
      closable: this.closable
    });
    this.isInit = true;
    // On check si des boutons on été ajouté par les composants app-modal-button
    if (!this.actions.length) {
      this.displayActions = true;
    }
    this.cdr.markForCheck();
  }

  /**
   * Ouvre la modal.
   * Des paramètres peuvent être passés en paramètres, ces même paramètres seront retournés lors
   * de la fermeture en paramètre aux observables approve | deny;
   * @param data
   */
  open(data?) {
    // Si des data sont passés en paramètre à l'ouverture de la modal
    if (data) {
      this.associateData(data);
    }
    // Si l'initialisation est terminé
    if (this.isInit) {
      // Si on a une autre modal
      if (this.nextModal) {
        // On indique qu'elles sont attachées
        this.isAttached = this.isCoupled = this.nextModal.isCoupled = true;
        // On set l'ouverture unique d'une modal (paramètre)
        $('.coupled.modal').modal({ allowMultiple : this.allowMultiple });
        // On attache l'evenement d'approbation pour l'ouverture de la prochaine modal
        this.nextModal.$modal.modal('attach events', this.approveButton.nativeElement);
      }
      // On affiche la modal
      this.$modal.modal('show');
    }
  }

  onApprove() {
    if (!this.isAttached) {
      this.close();
    }
    this.subjApprove.next(this.data);
  }

  onDeny() {
    this.close();
    this.subjDeny.next(this.data);
  }

  close() {
    this.$modal.modal('hide');
  }

  private associateData(data) {
    this.data = data;
    this.actions.forEach(el => {
      el.data = this.data;
    });
  }

}
