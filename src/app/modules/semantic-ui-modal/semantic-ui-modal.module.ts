import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiModalComponent } from './semantic-ui-modal.component';
import { ModalHeaderComponent } from './modal-header/modal-header.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { ModalButtonComponent } from './modal-button/modal-button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SemanticUiModalComponent,
    ModalHeaderComponent,
    ModalContentComponent,
    ModalButtonComponent
  ],
  exports: [
    SemanticUiModalComponent,
    ModalHeaderComponent,
    ModalContentComponent,
    ModalButtonComponent
  ]
})
export class SemanticUiModalModule { }
