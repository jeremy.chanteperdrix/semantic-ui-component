import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'sem-modal-button',
  templateUrl: './modal-button.component.html',
  styleUrls: ['./modal-button.component.scss']
})
export class ModalButtonComponent implements OnInit {
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  data: any;

  constructor() { }

  ngOnInit() {
  }

}
