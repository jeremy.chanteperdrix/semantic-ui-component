import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemanticUiModalComponent } from './semantic-ui-modal.component';

describe('SemanticUiModalComponent', () => {
  let component: SemanticUiModalComponent;
  let fixture: ComponentFixture<SemanticUiModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemanticUiModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemanticUiModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
