import { SemanticUiCalendarModule } from './semantic-ui-calendar/semantic-ui-calendar.module';
import { SemanticUiDatatableModule } from './semantic-ui-datatable/semantic-ui-datatable.module';
import { SemanticUiDropdownModule } from './semantic-ui-dropdown/semantic-ui-dropdown.module';
import { SemanticUiInputModule } from './semantic-ui-input/semantic-ui-input.module';
import { SemanticUiModalModule } from './semantic-ui-modal/semantic-ui-modal.module';
import { SemanticUiTableModule } from './semantic-ui-table/semantic-ui-table.module';

export const SemanticUiModules: any = [
  SemanticUiCalendarModule,
  SemanticUiDatatableModule,
  SemanticUiDropdownModule,
  SemanticUiInputModule,
  SemanticUiModalModule,
  SemanticUiTableModule,
];
