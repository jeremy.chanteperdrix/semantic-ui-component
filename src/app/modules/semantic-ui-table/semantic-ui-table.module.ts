import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemanticUiTableComponent } from './semantic-ui-table.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [SemanticUiTableComponent],
  exports: [SemanticUiTableComponent]
})
export class SemanticUiTableModule {
}
