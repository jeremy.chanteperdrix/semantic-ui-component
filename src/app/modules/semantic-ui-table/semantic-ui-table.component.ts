import {
  AfterContentInit, AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter,
  Input, OnChanges, Output, SimpleChanges
} from '@angular/core';

declare const $: any;

@Component({
  selector: 'sem-table',
  templateUrl: 'semantic-ui-table.component.html',
  styleUrls: ['semantic-ui-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SemanticUiTableComponent implements AfterViewInit, AfterContentInit, OnChanges {
  @Input() set headers(headers) { this._headers = headers; }
  @Input() set data(data) { this._data = data; }
  @Input() set options(options) { this._options = options; }

  @Output() public onChangeActivePage: EventEmitter<number> = new EventEmitter();
  @Output() public onChangeResultPerPage: EventEmitter<number> = new EventEmitter();
  @Output() public onSelectRow: EventEmitter<Object> = new EventEmitter();
  @Output() public onMultiSelectRow: EventEmitter<Object[]> = new EventEmitter();

  public dataPaged: any[] = [];
  public pages: number[] = [];
  public activePage = 1;
  public numRowSelect = 0;
  public toggle = false;

  private _headers: string[] = [];
  private _data: any[] = [];
  private _options: any;

  private _defaultOptions = {
    resultPerPage: {
      choices: [5, 10, 20, 40, 80],
      selected: 5,
      display: true,
      locked: false
    },
    selectable: true,
    multi: false
  };

  ngAfterViewInit() {
    $('.ui.dropdown').dropdown();
    $('.ui.checkbox').checkbox();
  }

  ngAfterContentInit() {
    this._options = Object.assign(Object.assign({}, this._defaultOptions), this._options);
    this._options.resultPerPage = Object.assign(Object.assign({}, this._defaultOptions.resultPerPage), this._options.resultPerPage || {});
    this._constructDataPaged();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['data'].isFirstChange() && changes['data'].currentValue.length !== changes['data'].previousValue.length) {
      this._constructDataPaged();
    }
  }

  get data() {
    return this._data;
  }

  get headers() {
    return this._headers;
  }

  get options() {
    return this._options;
  }

  private _constructDataPaged() {
    this.dataPaged = Array.from(this._data);
    this.dataPaged.forEach(el => {
      el._select = false;
    });
    this._constructPages();
    this._checkActivePage();
  }

  private _getCountPage() {
    let count = 0;
    for (let i = 0; i < this.dataPaged.length; i++) {
      if (i % this.options.resultPerPage.selected === 0) {
        count++;
      }
    }
    return count;
  }

  private _constructPages() {
    this.pages = [];
    for (let i = 0; i < this._getCountPage(); i++) {
      this.pages.push(i + 1);
    }
  }

  private _checkActivePage() {
    this.activePage = this.activePage > this._getCountPage() || this.activePage === 0 ? this._getCountPage() : this.activePage;
  }

  reloadTable(nb) {
    this._options.resultPerPage.selected = nb;
    this._constructPages();
    this._checkActivePage();
    this.reloadNumRowSelect();
    this.onChangeResultPerPage.emit(this._options.resultPerPage.selected);
  }

  emitOnChangeActivePage() {
    this.onChangeActivePage.emit(this.activePage);
  }

  gotoFirstPage() {
    this.activePage = 1;
    this.emitOnChangeActivePage();
  }

  gotoLastPage() {
    this.activePage = this._getCountPage();
    this.emitOnChangeActivePage();
  }

  gotoNextPage() {
    this.activePage = this.activePage + 1 > this._getCountPage() ? this.activePage : ++this.activePage;
    this.emitOnChangeActivePage();
  }

  gotoPreviousPage() {
    this.activePage = this.activePage - 1 < 1 ? this.activePage : --this.activePage;
    this.emitOnChangeActivePage();
  }

  reloadNumRowSelect() {
    this.numRowSelect = 0;
    this.dataPaged.forEach(el => {
      this.numRowSelect += el._select;
    });
  }

  toggleAll() {
    this.dataPaged.forEach(el => {
      el._select = this.toggle;
    });
    this.reloadNumRowSelect();
    this.multiSelectRow();
  }

  multiSelectRow() {
    const lineSelect = [];
    this.dataPaged.forEach(el => {
      if (el._select) {
        lineSelect.push(el);
      }
    });
    this.onMultiSelectRow.emit(lineSelect);
  }
}
