import { SemanticUiPopupDirective } from './semantic-ui-popup.directive';
import { SemanticUiDimmerDirective } from './semantic-ui-dimmer.directive';
import { SemanticUiAccordionDirective } from './semantic-ui-accordion.directive';
import { SemanticUiTabDirective } from './semantic-ui-tab.directive';
import { SemanticUiCheckboxDirective } from './semantic-ui-checkbox.directive';

export const Directives: any[] = [
  SemanticUiPopupDirective,
  SemanticUiDimmerDirective,
  SemanticUiAccordionDirective,
  SemanticUiTabDirective,
  SemanticUiCheckboxDirective,
];
