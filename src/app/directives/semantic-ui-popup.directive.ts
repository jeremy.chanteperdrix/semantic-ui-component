import { Directive, ElementRef, Input } from '@angular/core';

declare const $: any;

@Directive({ selector: '[sem-popup]'})
export class SemanticUiPopupDirective {
  @Input('sem-popup') content: string;
  @Input('popup-position') position: string;
  @Input('popup-title') title: string;
  @Input('popup-offset') offset: string;

  constructor(el: ElementRef) {
    setTimeout(() => {
      $(el.nativeElement).popup({
        position: (el.nativeElement.attributes['data-position'] || {}).textContent || this.position,
        title: (el.nativeElement.attributes['data-title'] || {}).textContent || this.title,
        content: (el.nativeElement.attributes['data-content'] || {}).textContent || this.content,
        offset: parseInt(this.offset, 10) || 0,
      });
    });
  }
}
