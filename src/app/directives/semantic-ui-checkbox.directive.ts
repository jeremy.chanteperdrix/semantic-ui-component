import { Directive, ElementRef, Input } from '@angular/core';

declare const $: any;

@Directive({ selector: '[sem-checkbox]' })
export class SemanticUiCheckboxDirective {
  constructor(el: ElementRef) {
    setTimeout(() => {
      $(el.nativeElement).checkbox();
    });
  }
}