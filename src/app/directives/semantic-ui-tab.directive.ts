import { Directive, ElementRef, Input } from '@angular/core';

declare const $: any;

@Directive({ selector: '[sem-tab]'})
export class SemanticUiTabDirective {
  @Input('sem-tab') effect: SemanticUiTabEffect;

  constructor(private el: ElementRef) {
    setTimeout(() => {
      // On récupère les éléments de type .ui.tab.segment à partir de l'élément courant
      const elements = [];
      let end = false;
      let tmp: HTMLElement;
      while (!end) {
        // Si on a pas d'élément dans le tableau
        if (elements.length === 0) {
          // Si on a un élément
          if (el.nativeElement.nextElementSibling) {
            tmp = el.nativeElement.nextElementSibling;
            // Si l'élément comporte les classes .ui.tab.segment
            if (tmp.className.includes('ui') && tmp.className.includes('tab') && tmp.className.includes('segment')) {
              // On ajoute l'élément dans le tableau
              elements.push(tmp);
            } else {
              end = true;
            }
          } else {
            end = true;
          }
        } else {
          // Sinon on regarde si le dernier élément du tableau à un élément suivant
          if (elements[elements.length - 1].nextElementSibling) {
            tmp = elements[elements.length - 1].nextElementSibling;
            // Si l'élément comporte les classes .ui.tab.segment
            if (tmp.className.includes('ui') && tmp.className.includes('tab') && tmp.className.includes('segment')) {
              // On ajoute l'élément dans le tableau
              elements.push(tmp);
            } else {
              end = true;
            }
          } else {
            end = true;
          }
        }
      }
      let $previous = $(elements.find(node => node.className.includes('active')));
      $('.item', el.nativeElement).tab({
        history: true,
        onVisible: () => {
          const $current = $(elements.find(node => node.className.includes('active')));
          $previous.show();
          $current.hide();
          $previous.transition({
            animation: this.effect || 'fade down',
            onComplete: () => {
              $current.transition(this.effect || 'fade down');
            }
          });
          $previous = $current;
        }
      });
    });
  }
}

export interface SemanticUiTabEffect {
  type: 'scale' | 'fade' | 'fade up' | 'fade down' | 'fade left' | 'fade right';
}
