import { Directive, ElementRef, Input } from '@angular/core';

declare const $: any;

@Directive({ selector: '[sem-dimmer]' })
export class SemanticUiDimmerDirective {
  @Input('sem-dimmer') on: string;

  constructor(el: ElementRef) {
    setTimeout(() => {
      $(el.nativeElement).dimmer({ on: this.on });
    });
  }
}
