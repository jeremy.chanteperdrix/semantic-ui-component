import { Directive, ElementRef } from '@angular/core'

declare const $: any;

@Directive({ selector: '[sem-accordion]'})
export class SemanticUiAccordionDirective {
  constructor(el: ElementRef) {
    setTimeout(() => {
      $(el.nativeElement).accordion();
    });
  }
}
