import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  valeur = 'hello';
  tab: any[];

  ngOnInit() {
    setTimeout(() => {
      this.tab = [
        { label: 'test', id: 0 },
        { label: 'hello', id: 1 },
        { label: 'frank', id: 2 },
        { label: 'bernard', id: 3 }
      ];
    }, 1000);
  }
}
