# SemanticUiComponent (beta)

Use **[ng2-semantic-ui](https://www.npmjs.com/package/ng2-semantic-ui)**.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.1.

## Components
* SemanticUiCalendar
* SemanticUiDatatable
* SemanticUiDropdown
* SemanticUiInput
* SemanticUiModal
* SemanticUiProgress
* SemanticUiTable

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Utiliser les composants

Pour utiliser les composants il faut que le framework Semantic-ui soit intallé dans le projet.
Semantic-ui doit être importer dans le fichier .angular-cli.

## .angular-cli.json
styles

    [
        "styles.scss",
        "../semantic/dist/semantic.min.css",
        "../node_modules/noty/src/noty.scss",
        "../node_modules/semantic-ui-calendar/dist/calendar.min.css",
        "./assets/styles/dataTables.semanticui.min.css",
        "./assets/styles/buttons.semanticui.min.css"
    ]

scripts

    [
        "../node_modules/jquery/dist/jquery.min.js",
        "../node_modules/noty/lib/noty.min.js",
        "../node_modules/datatables/media/js/jquery.dataTables.min.js",
        "./assets/scripts/dataTables.semanticui.min.js",
        "./assets/scripts/dataTables.select.min.js",
        "./assets/scripts/dataTables.buttons.min.js",
        "./assets/scripts/buttons.semanticui.min.js",
        "./assets/scripts/jszip.min.js",
        "./assets/scripts/pdfmake.min.js",
        "./assets/scripts/vfs_fonts.js",
        "./assets/scripts/buttons.html5.min.js",
        "./assets/scripts/buttons.print.min.js",
        "./assets/scripts/buttons.colVis.min.js",
        "../semantic/dist/semantic.min.js",
        "../node_modules/moment/min/moment.min.js",
        "../node_modules/jquery-address/src/jquery.address.js",
        "../node_modules/semantic-ui-calendar/dist/calendar.min.js",
        "../node_modules/highcharts/highcharts.js"
    ]

## Semantic-ui `gulp build`
Problème de version gulp sur minification des js (gulp build).

Commenter les lignes `preserveComments` dans `tasks/config/tasks.js`.

    /* Minified JS Settings */
    uglify: {
      mangle           : true
      // preserveComments : 'some'
    },
    
    /* Minified Concat JS */
    concatUglify: {
      mangle           : true
      // preserveComments : false
    }